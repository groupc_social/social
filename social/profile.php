<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'view/comment.php';
include_once 'view/post.php';
include_once 'view/tag.php';
include_once 'view/control.php';
sec_session_start();

$user = $_SESSION['user_id'];

$mysqli = new mysqli('127.0.0.1', 'root', '','social');
?>
<html>
  <head>
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="styleProfile.css">
       <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">

       <style>
       .eachPost{
         font-size: 20px;
           font-family: "Comic Sans MS";
       }
       .user_post{
         font-size: 30px !important;
       }
       </style>

    <script>
        function myFunction2() {
            document.getElementById("myDropdown2").classList.toggle("show2");
        }

        // Close the dropdown menu if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content2");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show2')) {
                openDropdown.classList.remove('show2');
              }
            }
          }
        }
    </script>

    <div class="headBar">
      <a href="newfeed.php"><div class="headbtn" style="left:0;width:18.4%;"><image src="logo2.png" style="width:40%;height:90%;margin-top:0.7%;"></div></a>

      <div class="headbtn dropbtn"  onclick="myFunction2()" style="left:18.4%;">add friend
          <div id="myDropdown2" class="dropdown-content2">
            <?php
            $friend_add = new Control;
            $friend_add_ar = $friend_add->add_friend($user);
            foreach($friend_add_ar as $row1)
            {

                $user_friend2 = $row1->user_id;
                ?>

                <?php
                $user_friend = "SELECT * FROM me WHERE user_id = $user_friend2  ";

                $user_friend_arr = $mysqli->query($user_friend);
                while($row12 = $user_friend_arr->fetch_object()){


                 ?>

                <img src="upload/<?php echo $row12->photo ?>" style="width:20%;height:50%;border-radius:100px;margin-top:3%;">
                <a href="database/addFriend.php?id=<?php echo $user_friend2?>"><div style="margin-top:-4%"><?php echo $row12->name ?></div></a><?php
            }
          }
             ?>
          </div>

      </div>

    </div>



  </head>

  <body>
    <div class="menuTab">
      <!-- เเก้ไข้ได้ -->
      <?php
      if(isset($_GET['id']))
      {
      $id = $_GET['id'];
      }
      else {
        $id = $user;
      }
      if($id != $user)
      {
        ?>
        <a href="showAbout.php"><div class="menubtn">About me</div></a>
        <?php
      }
      else {
        ?>
          <a href="editAbout.php"><div class="menubtn">About me</div></a>
        <?php
      }?>
      <a href="profile.php?id=<?php echo $user ?>"><div class="menubtn">My profile</div></a>
      <a href="friend.php?id=<?php echo $user ?>"><div class="menubtn">Friends</div></a>
      <a href="index2.php?id=<?php echo $user ?>"><div class="menubtn">Post movie</div></a>
      <a href="database/setstatus.php?id=<?php echo $user?>"><div class="menubtn" style="position:absolute;bottom:0;background-color:rgb(149, 149, 149)">Log Out</div></a>
    </div>
    <div class="profile">
      <?php
      $name_me = new Control;
      $name_me_ar = $name_me->show_detail_me($id);
      foreach($name_me_ar as $row)
      {
      ?>
      <!-- ////////////////////////////////////// -->
      <div class="headProfile">
        <div class="profilePic">
        <img src="upload/<?php echo $row->photo?> " alt="Nature" style="width:100%">
        </div>
        <a href="#profile"><div id="name"><?php echo $row->name?></div></a>
          <?php } ?>
      </div>

      <div class="menuProfile">
        <a href="profile.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="border-radius:0 0 0 6px;">Timeline</div></a>
        <?php
        if($id != $user)
        {
          ?>
          <a href="showAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }
        else {
          ?>
            <a href="editAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }?>

        <a href="friend.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="left:40%;">Friends</div></a>

      </div>

      <div class="post">
        <form method="post" action="database/insertPost.php">
          <input type="text" placeholder=" write a post" id="post" name="detail">
          <input type="text" placeholder=" # Hash Tag" id="hashtag"  name="tag" >
          <input type="hidden" name="date" value="<?php echo '0.00' ?>">
          <input type="hidden" name="user" value="<?php echo $user ?>">
          <input class="button" type="submit">
        </form>
      </div>

      <div class="showPost">
        <?php
          $show_all = new Post;
          $show_all_arr = $show_all->show_post_all($id);
          foreach($show_all_arr as $row9)
          {
            if($row9->table_name == 'post')
            {
              $post_id = $row9->id;
              $user = "SELECT * FROM post WHERE id = $post_id  ";

              $result3 = $mysqli->query($user);
              while($row20 = $result3->fetch_object()){


              ?>

                  <div class="eachPost">
                    <?php

                    $user5 = "SELECT * FROM me WHERE user_id = $id  ";
                    $result35 = $mysqli->query($user5);
                    //print_r($result35);
                    while($row31 = $result35->fetch_object()){
                    ?>

                    <img src="upload/<?php echo $row31->photo ?>" style="width:8%;height:7%;margin-left:3%;margin-top:2.5%;"> <!-- ใส่รูปโปรไฟล์ -->
                    <div style="text-align:left;margin-top:-7%;margin-left:13%;">
                      <?php echo $row31->name;  ?>
                    </div>

                    <?php } ?>
                    <center>
                        <div class="nameMovie"><?php echo $row20->detail; ?></div>
                        <div class="postmassage">
                        <?php

                        $user2 = "SELECT * FROM tag_user WHERE post_id = $post_id  ";
                        $result5 = $mysqli->query($user2);
                        while($row30 = $result5->fetch_object()){

                          ?>     <div ><?php echo '#'.$row30->tag; ?></div> <?php
                        }
                        ?>
                        </div>



                    </center>
                  </div> <?php
                }

            }else {
              ?>      <div class="eachPost">
                <?php

                $user5 = "SELECT * FROM me WHERE user_id = $id  ";
                $result35 = $mysqli->query($user5);
                //print_r($result35);
                while($row31 = $result35->fetch_object()){
                ?>

                <img src="upload/<?php echo $row31->photo ?>" style="width:8%;height:7%;margin-left:3%;margin-top:2.5%;"> <!-- ใส่รูปโปรไฟล์ -->
                <div style="text-align:left;margin-top:-7%;margin-left:13%;">
                  <?php echo $row31->name;  ?>
                </div>

                <?php } ?>
                    <center>
                      <?php
                      $post_movie_id = $row9->id;
                      $user50 = "SELECT * FROM post_movie WHERE id = $post_movie_id  ";
                      $result50 = $mysqli->query($user50);
                      while($row22 = $result50->fetch_object()){


                        ?>
                        <div class="manuED"  ><i class="fa fa-pencil" ></i></div>
                        <div class="myDropdown dropdown-content" >
                          <a href="editPost.php?id=<?php echo $row22->id?>"><div class="edit" ><span style="margin-left: 17%;">Edit</a></span></div>
                          <a href="database/deletePost_movie.php?id=<?php echo $row22->id?>"><div class="delete"><span style="margin-left: 17%;"> Delete</a></span></div>
                        </div>
                        <div class="nameMovie"><?php echo $row22->detail;  ?></div>

                        <?php
                        $ddd = $row22->movie_id;

                        $user500 = "SELECT * FROM post_admin WHERE id =  $ddd ";
                        $result500 = $mysqli->query($user500);
                        while($row220 = $result500->fetch_object()){
                          ?>
                          <div class="postmassage"><?php echo $row220->title ?></div>
                          <div class="postMovie">  <img src="upload/<?php echo $row220->photo ?> " alt="Nature" style="width:786px;height:610px;"></div>
                           <?php
                        }
                        ?>


                        <!-- /////////////////////////////////////////////////////////////////////////////////////
                        ///////////////////////////////////////////////////////////////////////////////////// -->
                        <div class="comment" >
                          <br><br>
                          <?php
                          $show_all_comment = new Comment;
                          $show_all_comment_arr = $show_all_comment->show_comment($row22->id,$user);
                          foreach($show_all_comment_arr as $row90)
                          {

                            $user_comment = "SELECT * FROM members WHERE id =  $row90->user_id ";
                            $user_comment_arr = $mysqli->query($user_comment);
                            while($row234 = $user_comment_arr->fetch_object()){?>
                              <div style="font-size:1.5em;margin-left:0%;font-weight:bold;display:block;width:70%;text-align:left;border-radius:5px;height:5%;background-color:#B2BABB;line-height:60px;"><font style="margin-left:5%;"><?php echo $row234->username.'</font> ';
                            }
                          ?><font style="margin-left:10%;font-weight:normal"><?php echo $row90->comment.'</font></div><br>';
                          }
                           ?>
                          <form action="database/insertComment.php?id=<?php echo $user ?>" method="post">
                            <div>
                                <?php
                                //echo "$user";
                                $user_id = $_SESSION['user_id'];
                                $user_comment2 = "SELECT * FROM members WHERE id =  '$user_id' ";
                                $user_comment_arr2 = $mysqli->query($user_comment2);
                                while($row2342 = $user_comment_arr2->fetch_object()){?>

                                    <font style="text-shadow: 1px 1px 2px grey, 0 0 25px grey, 0 0 5px black;margin-left:-11%;margin-top:0%;font-size:1.7em"> <?php echo $row2342->username.'</font>';
                                ?>
                                <?php }
                                 ?>

                            <input type="text" name="comment" style="width:400px; height: 50px;margin-left: 3%; margin-top:5%;font-size:1em;border-radius:5px;">
                            <input type="hidden" name="post" value="<?php echo $row22->id ?>">
                            <input type="submit" style="width:100px; height: 30px; margin-left:55%;  margin-top:-4%; font-size: 1.2em;" class="button2" value="comment">
                            </div>
                          </form>

                        </div>


                    </center>
                  </div> <?php
            }
          }
          ///// โชว์โพสทั้งหมด
          ?>


        <?php } ?>
      </div>

    </div>


    <div class="online">
      <?php
      $mysqli = new mysqli('127.0.0.1', 'root', '','social');
      $login = "SELECT * FROM friend WHERE user_id='$user_id' AND status='yes' ";
      $result = $mysqli->query($login);
        while($row6=$result->fetch_assoc()){
            $friend_id_s = $row6['friend_id'];
            $login_s = "SELECT * FROM members WHERE id='$friend_id_s' AND status ='on' ";
            $result2 = $mysqli->query($login_s);
                while($row7=$result2->fetch_assoc()){
                  ?>   <a href="message.php?id=<?php echo $row7['id'] ?> "><div class="friendOnline"> <font style="color:green"> &#9679;</font> <?php echo $row7['username']?></div></a> <?php
                }
        }
       ?>

    </div>

  </body>
      <script>
        $(document).ready(function() {
          $('.manuED').click(function() {
              $(this).next().toggle();
              // $('.myDropdown').show()
          });

          $('.eachPost').hover(
              function(){
                  $(this).find('.fa').show();
              },
              function(){
                  $(this).find('.fa').hide();
              }
          );
          $('.edit').click(
              function(){
              $('.myDropdown').hide();
          });
          $('.delete').click(
              function(){
              $('.myDropdown').hide();
          });
        });
      </script>
      <script>

          // Close the dropdown if the user clicks outside of it
          window.onclick = function(event) {
            if (!event.target.matches('.fa')) {

              var dropdowns = document.getElementsByClassName("dropdown-content");
              var i;
              for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                  openDropdown.classList.remove('show');

                }
              }
            }
          }
      </script>
</html>
