
<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>log in</title>
	<link rel="stylesheet" href="style.css" media="all">

      <link href="themes/1/slider.css" rel="stylesheet" type="text/css" />
      <script src="themes/1/slider.js" type="text/javascript"></script>
      <link href="generic.css" rel="stylesheet" type="text/css" />
			<script type="text/JavaScript" src="js/sha512.js"></script>
			<script type="text/JavaScript" src="js/forms.js"></script>

</head>
<body>
	<!--container starts-->
        <div class="container">
            <!--header wrap starts-->
            <div id="head_warp">
            	<!--header starts-->
            	<div id="header">
            		<img  src="logo2.png" style="float:left; width:13%"/>

								<?php
				        if (isset($_GET['error'])) {
				            echo '<p class="error">Error Logging In!</p>';
				        }
				        ?>
				        <form action="includes/process_login.php" method="post" name="login_form" style="color: white;">
				            Email: <input type="text" name="email" />
				            Password: <input type="password"
				                             name="password"
				                             id="password"/>
				            <input type="button"
				                   value="Login"
				                   onclick="formhash(this.form, this.form.password);" />
				        </form>

            	</div>
            	<!--header ends-->
            </div>
            <!--header wrap ends-->

            <!--Content area Starts-->
            <div id="content">



                        <div id="slider" style="float: left;">

                              <img src="images/1.jpg" alt="Hello."/>
                              <img src="images/2.jpg" alt="Welcome to MOVIE SQUARE." />
                              <img src="images/3.jpg" alt="Come with me." />
                              <img src="images/4.jpg" alt="I love you." />
                              <img src="images/5.jpg" alt="Jub Jub." />
                        </div>


            	<div id="form2">
								<form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" style="color: white;">
				            Username: <input style="    margin-left: 10%;" type='text' name='username' id='username' /><br>
				            Email: <input  style="    margin-left: 15%;" type="text" name="email" id="email" /><br>
				            Password: <input style="    margin-left: 11%;" type="password"
				                             name="password"
				                             id="password"/><br>
				            Confirm password: <input type="password"
				                                     name="confirmpwd"
				                                     id="confirmpwd" /><br>
				            <input style="    margin-left: 31%; width: 42%; height: 0%; cursor:pointer;" type="button"
				                   value="Register"
				                   onclick="return regformhash(this.form,
				                                   this.form.username,
				                                   this.form.email,
				                                   this.form.password,
				                                   this.form.confirmpwd);" />
				        </form>
            	</div>
            </div>
            <!--Content area ends-->

            <div id="footer">
            	<h2>&copy; MOVIE SQUARE ISNE#3 2016</h2>
            </div>
        </div>
        <!--container ends-->

</body>
</html>
