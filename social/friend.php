<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'view/comment.php';
include_once 'view/post.php';
include_once 'view/tag.php';
include_once 'view/control.php';
sec_session_start();

$user = $_SESSION['user_id'];

?>
<html>
  <head>
    <title>Friends</title>
    <link rel="stylesheet" type="text/css" href="styleProfile.css">
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <script>
        function myFunction2() {
            document.getElementById("myDropdown2").classList.toggle("show2");
        }

        // Close the dropdown menu if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content2");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show2')) {
                openDropdown.classList.remove('show2');
              }
            }
          }
        }
    </script>

    <div class="headBar">
      <a href="newfeed.php"><div class="headbtn" style="left:0;width:18.4%;"><image src="logo2.png" style="width:40%;height:90%;margin-top:0.7%;"></div></a>

    </div>


  </head>

  <body>
    <div class="menuTab">
      <!-- เเก้ไข้ได้ -->
      <?php
      if(isset($_GET['id']))
      {
      $id = $_GET['id'];
      }
      else {
        $id = $user;
      }
      if($id != $user)
      {
        ?>
        <a href="showAbout.php"><div class="menubtn">About me</div></a>
        <?php
      }
      else {
        ?>
          <a href="editAbout.php"><div class="menubtn">About me</div></a>
        <?php
      }?>
      <a href="profile.php?id=<?php echo $user ?>"><div class="menubtn">My profile</div></a>
      <a href="friend.php?id=<?php echo $user ?>"><div class="menubtn">Friends</div></a>

      <a href="database/setstatus.php?id=<?php echo $user?>"><div class="menubtn" style="position:absolute;bottom:0;background-color:rgb(149, 149, 149)">Log Out</div></a>
    </div>

    <div class="profile" style="margin-left:1.5%;" >
      <br><br>
      <center><font style="font-size:2em;text-shadow: 0 0 20px #ffffff;"> Friends </font></center>
      <center>

        <?php
        $friend_show = new Control;
        $friend_show_ar = $friend_show->show_friend($user);
        foreach($friend_show_ar as $row2)
        {  // วนลูป ลอง css เฉยๆนะ 55555

          $friend_id = $row2->friend_id;
          $friend_show_all = new Control;
          $friend_show_all_ar = $friend_show_all->show_friend_all($friend_id);
          foreach($friend_show_all_ar as $row3)
          {

        //list($width, $height) = getimagesize($row3->photo); // เอาชื่อ+นามสกุลไฟล ของรูปโปรไฟล์แต่ละคน มาใส่ในฟังก์ชั่นนี้นะ
        // เปลี่ยนขนาดรูปภาพอัตโนมัติ 5555
         ?>
      <a href="#friend"><div class="polaroid">

        <div class="polaroid2"><img src="upload/<?php echo $row3->photo ?>" <?php echo ($height>$width)?'class="portrait"':''; ?>></div>
        <div class="container">
          <p><!--Name of Freinds --><?php echo $row3->name ?></p>
        </div>

      </div></a>
      <?php
    }

    } ?>
    </div>


    <div class="online">
      <?php
      $mysqli = new mysqli('127.0.0.1', 'root', '','social');
      $login = "SELECT * FROM friend WHERE user_id='$user' AND status='yes' ";
      $result = $mysqli->query($login);
        while($row6=$result->fetch_assoc()){
            $friend_id_s = $row6['friend_id'];
            $login_s = "SELECT * FROM members WHERE id='$friend_id_s' AND status ='on' ";
            $result2 = $mysqli->query($login_s);
                while($row7=$result2->fetch_assoc()){
                  ?>   <a href="message.php?id=<?php echo $row7['id'] ?> "><div class="friendOnline"> <font style="color:green"> &#9679;</font> <?php echo $row7['username']?></div></a> <?php
                }
        }
       ?>

    </div>
    <script>
      // var w = $('.polaroid2 img').width;
      // var h = $('.polaroid2 img').height;
      // if(h > w) {
      //   $()
      // }
    </script>
  </body>
</html>
