<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'view/comment.php';
include_once 'view/post.php';
include_once 'view/tag.php';
include_once 'view/control.php';
sec_session_start();

$user = $_SESSION['user_id'];
if(isset($_GET['id']))
{
$id = $_GET['id'];
}
else {
  $id = $user;
}
?>
<html>
  <head>
    <title>About me</title>
    <link rel="stylesheet" type="text/css" href="styleProfile.css">
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <script>
        function myFunction2() {
            document.getElementById("myDropdown2").classList.toggle("show2");
        }

        // Close the dropdown menu if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content2");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show2')) {
                openDropdown.classList.remove('show2');
              }
            }
          }
        }
    </script>

    <div class="headBar">
      <a href="newfeed.php"><div class="headbtn" style="left:0;width:18.4%;"><!--<img src="1.png">--></div>
      <!-- <div class="headbtn dropbtn"  onclick="myFunction()" style="left:18.4%;">add friend
          <div id="myDropdown" class="dropdown-content">
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
          </div>
      </div> -->
    </div>


  </head>

  <body>
    <div class="menuTab">
      <!-- เเก้ไข้ได้ -->
      <a href="profile.php?id=<?php echo $user ?>"><div class="menubtn">My profile</div></a>
      <a href="showAbout.php"><div class="menubtn">About me</div></a>
      <a href="friend.php"><div class="menubtn">Friends</div></a>

      <div class="menubtn" style="position:absolute;bottom:0;background-color:rgb(149, 149, 149);">Log Out</div>
    </div>

    <div class="profile">
      <?php
      $name_me = new Control;
      $name_me_ar = $name_me->show_detail_me($user);
      foreach($name_me_ar as $row)
      {
      ?>
      <!-- ////////////////////////////////////// -->
      <div class="headProfile"></b>
        <div class="profilePic">
        <img src="upload/<?php echo $row->photo?> " alt="Nature" style="width:100%">
        </div>
        <a href="#profile"><div id="name"><?php echo $row->name?></div></a>
          <?php } ?>
      </div>

      <div class="menuProfile">
        <a href="profile.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="border-radius:0 0 0 6px;">Timeline</div></a>
        <?php
        if($id != $user)
        {
          ?>
          <a href="showAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }
        else {
          ?>
            <a href="editAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }?>

        <a href="friend.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="left:40%;">Friends</div></a>
      </div>

      <?php
      $show_me = new Control;
      $show_me_arr = $show_me->show_detail_me($user);
      foreach($show_me_arr as $row4)
      {
       ?>
      <div class="boxAbout">
        <div class="showData">Name : <font style="color:#343f50;margin-left:20%;text-shadow: 0 0 5px #2eb191;"><?php echo $row4->name ?></font></div>
        <div class="showData">Email : <font style="color:#343f50;margin-left:20%;text-shadow: 0 0 5px #2eb191;"><?php echo $row4->email ?><!-- Email --></font></div>
        <div class="showData">Facebook : <font style="color:#343f50;margin-left:16.5%;text-shadow: 0 0 5px #2eb191;"><?php echo $row4->fb ?><!-- Facebook--></font></div>
      </div>
    </div>
    <?php } ?>

    <div class="online">
      <?php
      $mysqli = new mysqli('127.0.0.1', 'root', '','social');
      $login = "SELECT * FROM friend WHERE user_id='$user' AND status='yes' ";
      $result = $mysqli->query($login);
        while($row6=$result->fetch_assoc()){
            $friend_id_s = $row6['friend_id'];
            $login_s = "SELECT * FROM members WHERE id='$friend_id_s' AND status ='on' ";
            $result2 = $mysqli->query($login_s);
                while($row7=$result2->fetch_assoc()){
                  ?>   <a href="message.php?id=<?php echo $row7['id'] ?> "><div class="friendOnline"> <font style="color:green"> &#9679;</font> <?php echo $row7['username']?></div></a> <?php
                }
        }
       ?>

    </div>

  </body>
</html>
