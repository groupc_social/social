<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'view/comment.php';
include_once 'view/post.php';
include_once 'view/tag.php';
include_once 'view/control.php';
sec_session_start();

$user = $_SESSION['user_id'];
if(isset($_GET['id'])){
  $id = $_GET['id'];
}
else {
  $id = '1';
}

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="mycss.css"/>
<link rel="stylesheet" type="text/css" href="chick.css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.manu_left{
  position: absolute;
  width: 70%;
  height: 200px;
  box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
  margin-left: 13%;
  margin-top: 7%;

}
.manu_left2{
  width: 70%;
  height: 200px;
      box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
  /* top: 240px; */
  margin-left: 13%;
  margin-top: 57%;
}
.manu_left3{
  position: absolute;
  width: 70%;
  height: 29%;
      box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
  /* top: 480px; */
  /* width: 70%; */
  /* height: 200px; */
  /* background-color: red; */
  /* top: 240px; */
  margin-left: 13%;
  margin-top: 22%;
}

div.menubtn{
  width: 100%;
  height: 7%;
  display: block;
  text-align: center;
  line-height: 60px;
  cursor: pointer;
  font-size: 18px;
  color: #c5c5c5;
}

div.menubtn:hover{
  color: white;
  -webkit-transition-duration: 0.4s;
  transition-duration: 0.4s;
}
</style>
<link rel="stylesheet" href="jquery-ui.css">
<script src="jquery-3.1.1.min.js"></script>
<script src="jquery-ui.js"></script>
<script>
$(document).ready(function() {
$('.inputDetail_invite ').click(function() {
   var photo = $(this).attr('photo');

// alert(photo);
   var detail = $(this).attr('detail');
   var title = $(this).attr('name');
   var id = $(this).attr('movie');
   $('#inputID').val(id);
   $('#inputDate_detail2').val(photo);
   $('#inputDetail_detail2').val(detail);
   $('#inputTitle_detail2').val(title);

   $('#photo_show').attr('src', 'upload/'+photo );

  document.getElementById('id01').style.display='block';
});

// $('.inputDetail_invite2 ').click(function() {
//    var photo = $(this).attr('phpto');
//    var detail = $(this).attr('detail');
//    var title = $(this).attr('name');
//    var id = $(this).attr('movie');
//    $('#inputID2').val(id);
//    $('#inputDate_detail3').val(photo);
//    $('#inputDetail_detail3').val(detail);
//    $('#inputTitle_detail3').val(title);
//
//
//   document.getElementById('id02').style.display='block';
// });

$('#responsive').lightSlider({
        item:4,
        loop:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
        ]
    });

  });
</script>
</head>
<style>
.box{
  position: absolute;
  height: 5%;
  width: 34%;
  background: rgb(220, 222, 222);
  border-radius: 5px 5px 5px 5px;
  margin-top: 3%;
  margin-left: 4%;
}
.boxMo {
    position: absolute;
    background: rgb(221, 221, 221);
    box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
    width: 77%;
    height: 53%;
    margin-top: 95px;
    margin-left: 108px;
}
.boxMess {
    position: absolute;
    background: rgb(221, 221, 221);
    width: 88%;
    height: 8%;
    margin-top: 60%;
    margin-left: 6%;
    border-radius: 5px 5px 5px 5px;
}
.boxRate {
  position: absolute;
  background: rgb(221, 221, 221);
  width: 8%;
  height: 5.0%;
  margin-top: 3%;
  right: 41%;
  border-radius: 5px;
}
.spoil{
  position: absolute;
  height: 7%;
  width: 88%;
  background: rgb(220, 222, 222);
  border-radius: 5px 5px 5px 5px;
  margin-top: 70%;
  margin-left: 6%;
}
.postup{
  position: absolute;
    height: 6%;
    width: 71%;
    background: rgb(220, 222, 222);
    border-radius: 5px 5px 5px 5px;
    margin-top: 79%;
    margin-left: 6%;
}
.submitup{
  position: absolute;
  background: rgb(221, 221, 221);
  width: 11%;
  height: 6%;
  margin-top: 79%;
  margin-left: 80%;
  border-radius: 5px;
}
.top{
    background: rgba(35, 43, 47, 0.51);
    box-shadow: 1px 1px 3px 3px rgba(44, 38, 38, 0.32);
    border-radius: 6px 6px 6px 6px;
    /* position: relative; */
    position: absolute;
    width: 92%;
    height: 80%;
    margin-top: 4%;
    margin-left: 4%;
}
.insidemovie{
  position: absolute;
  margin-left: 14%;
  margin-top: 3%;
  width: 71%!important;
  height: 86%;
  box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
  cursor: pointer;

}
.rre{
  font-size: 50px;
  text-align: center;
  font-family: "Comic Sans MS";
  color: white;
}
.manu_left{
  background-color: #F44336;
}
.manu_left2{
  background-color: #2196F3;
}
.manu_left3{
  background-color: Orange;
}
</style>
<body>
    <ul class="tab">
        <div class="boxmovie">

            </li></a>
            <form>
                <!-- <input class="search" type="text" placeholder="  Search..." required>
                <input class="button" type="button" value="Search"> -->
            </form>
            <li>
                <span class="dayteb buttonadd"><a href="newfeed.php" >HOME</a></span>
                <span class="weekteb buttonadd"><a href="weekview.php">NEWFEED</a></span>

            </li>
        </div>
    </ul>
    <div class="newfeed">
        <div class="left">
          <a href="newfeed.php?id=<?php echo $id ?>"><div class="manu_left rre">
            Newfeed
          </div></a>

          <a href="profile.php?id=<?php echo $id ?>"><div class="manu_left2 rre">
            Profile
          </div></a>

          <a href="friend.php?id=<?php echo $id ?>"><div class="manu_left3 rre">
            Friend
          </div></a>
          <a href="database/setstatus.php?id=<?php echo $user?>"><div class="menubtn" style="position:absolute;bottom:0;background-color:#5f5f5f">Log Out</div></a>
        </div>
        <!-- <div class="posi"> -->
            <div class="movie">
              <!-- ลองใส่รูปภาพ -->
               <div class="insidemovie">
                 <?php
                 $show_movie_admin = new Post;
                 $show_movie_admin_ar = $show_movie_admin->show_post_admin();
                 foreach($show_movie_admin_ar as $row)
                 {
                   ?>   <img src="upload/<?php echo $row->photo?> " alt="Nature" style="width:1290px; height:694px;"> <?php
                   break;
                 }
                 ?>

               </div>

           </div>
           <div class="tabmovie">
               <i class="fa fa-chevron-circle-left" style="margin-left: 3%; margin-top: 9%; cursor: pointer; font-size: 66px;"></i>
               <div class="bigmovies" id="responsive">
                   <?php
                   $it=1;
                   foreach($show_movie_admin_ar as $row)
                   {
                      if($it<7)
                      {
                        if($id==1)
                        {
                          ?> <div class="movies inputDetail_invite2"  style="margin:8px;" movie="<?php echo $row->id ?>" name="<?php echo $row->title ?>" detail="<?php echo $row->detail ?>" photo="<?php echo $row->photo ?>">
                            <img src="upload/<?php echo $row->photo?> " alt="Nature" style="width:225px; height:249px;"> <?php
                              ?>  </div><?php
                        }
                        else {
                          ?> <div class="movies inputDetail_invite"  style="margin:8px;" movie="<?php echo $row->id ?>" name="<?php echo $row->title ?>" detail="<?php echo $row->detail ?>" photo="<?php echo $row->photo ?>">
                            <img src="upload/<?php echo $row->photo?> " alt="Nature" style="width:225px; height:249px;"> <?php
                              ?>  </div><?php
                        }
                      }
                        $it++;
                      }
                        ?>

               </div>
               <i class="fa fa-chevron-circle-right" style="margin-left: 92%; margin-top: -3%; cursor: pointer!important; font-size: 66px;"></i>
           <!-- </div> -->
        </div>
    </div>
    <div id="id01" class="modal" style="display:none;">

      <form class="modal-content animate" action="database/insertPost_movie_user.php" method="post" >
            <div class="top">
               <input class="box" type="text" name="title" placeholder=" name movie"  id="inputTitle_detail2">


               <div class="boxMo"><img id="photo_show" alt="Nature" style="width:760px; height:469px;"></div>

               <input class="boxMess" type="text" name="detail" placeholder="Massage" id="inputDetail_detail2">


               <input class="boxRate" type="text" name="rate" placeholder="Rate">
               <input class="spoil" type="text" name="spoil" placeholder="Extoller">
                <input class="postup" type="text" name="post" placeholder="Post">
               <input type="hidden" name="movie" id="inputID">
               <input type="hidden" name="date" value="<?php echo '0.00' ?>">
               <input type="hidden" name="id" value="<?php echo $id ?>">
                <input class="submitup" type="submit" >


      </form>
  </div>


  <!-- <div id="id02" class="modal" style="display:none;">

    <form class="modal-content animate"  method="post" >
          <div class="top">
             <input class="box" type="text" name="title" placeholder=" name movie"  id="inputTitle_detail3">
             <div class="boxMo"><img src="upload/13925032_516009651926720_5229263359200885879_n.jpg" alt="Nature" style="width:738px; height:449px;"></div>
                <input class="boxMess" type="text" name="detail" placeholder="Massage" id="inputDetail_detail3">
             <input type="hidden" name="id" id="inputID2">


    </form>
  </div> -->
    <script>

        // Get the modal
        var modal = document.getElementById('id01');

        var modal2 = document.getElementById('id02');

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
            if (event.target == modal2) {
                modal2.style.display = "none";
            }
        }
    </script>
</body>
</html>
