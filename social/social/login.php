
<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>log in</title>
	<link rel="stylesheet" href="style.css" media="all">

      <link href="themes/1/slider.css" rel="stylesheet" type="text/css" />
      <script src="themes/1/slider.js" type="text/javascript"></script>
      <link href="generic.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<!--container starts-->
        <div class="container">
            <!--header wrap starts-->
            <div id="head_warp">
            	<!--header starts-->
            	<div id="header">
            		<img  src="logo.png" style="float:left;"/>

            		<form  name="login_form" action="profile.php" id="form1">

                              <table>
                                    <tr>
                                          <td><strong>Email:</strong></td>
                                          <td><strong>Password:</strong></td>
                                    </tr>

                                    <tr>
                                          <td><input type="email" name="email" placeholder="Username@hotmail.com" ></td>

                                          <td><input type="password" name="password" placeholder="**********"></td>

                                          <td><a href=""><button name="login">Login</button></a></td>
                                    </tr>






                              </table>
            		</form>
            	</div>
            	<!--header ends-->
            </div>
            <!--header wrap ends-->

            <!--Content area Starts-->
            <div id="content">



                        <div id="slider" style="float: left;">

                              <img src="images/1.jpg" alt="Hello."/>
                              <img src="images/2.jpg" alt="Welcome to MOVIE SQUARE." />
                              <img src="images/3.jpg" alt="Come with me." />
                              <img src="images/4.jpg" alt="I love you." />
                              <img src="images/5.jpg" alt="Jub Jub." />
                        </div>


            	<div id="form2">
              <form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
            		<h2>Sign Up Here</h2><br>
            			<table>
            				<tr>
            					<td align="right"><p>Name:</p></td>
            					<td>
            						<input type="text" name="user_name" placeholder="name" required="required"/>
            					</td>
            				</tr>

            				<tr>
            					<td align="right"><p>Email:</p></td>
            					<td>
            						<input type="email" name="user_email" placeholder="Enter your email" required="required"/>
            					</td>
            				</tr>

                                    <tr>
                                          <td align="right"><p>Password:</p></td>
                                          <td>
                                                <input type="password" name="user_pass" placeholder="password" required="required"/>
                                          </td>
                                    </tr>

                                    <tr>

                                    </tr>

            			</table>
                              <center><button name="submit">Submit</button></center>
            		</form>
            	</div>
            </div>
            <!--Content area ends-->

            <div id="footer">
            	<h2>&copy; MOVIE SQUARE ISNE#3 2016</h2>
            </div>
        </div>
        <!--container ends-->

</body>
</html>
