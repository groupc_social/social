<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'view/comment.php';
include_once 'view/post.php';
include_once 'view/tag.php';
include_once 'view/control.php';
sec_session_start();

$user = $_SESSION['user_id'];

?>
<html>
  <head>
    <title>Edit about me</title>
    <link rel="stylesheet" type="text/css" href="styleProfile.css">
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <script>
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown menu if the user clicks outside of it
    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    </script>

    <div class="headBar">
      <a href="newfeed.php"><div class="headbtn" style="left:0;width:18.4%;"><!--<img src="1.png">-->Logo</div>
      <!-- <div class="headbtn dropbtn"  onclick="myFunction()" style="left:18.4%;">add friend
          <div id="myDropdown" class="dropdown-content">
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
          </div>
      </div> -->
    </div>


  </head>

  <body>
    <div class="menuTab">
      <!-- เเก้ไข้ได้ -->
      <a href="editAbout.php?id=<?php echo $user ?>"><div class="menubtn">About me</div></a>
      <a href="friend.php?id=<?php echo $user ?>"><div class="menubtn">Friends</div></a>

      <div class="menubtn" style="position:absolute;bottom:0;background-color:rgb(149, 149, 149);">Log Out</div>
    </div>

    <div class="profile">
      <?php
      $name_me = new Control;
      $name_me_ar = $name_me->show_detail_me($user);
      foreach($name_me_ar as $row)
      {
      ?>
      <!-- ////////////////////////////////////// -->
      <div class="headProfile"> Cover Photo</b>
        <div class="profilePic">
        <img src="upload/<?php echo $row->photo?> " alt="Nature" style="width:100%">
        </div>
        <a href="#profile"><div id="name"><?php echo $row->name?></div></a>
          <?php } ?>
      </div>

      <div class="menuProfile">
        <a href="profile.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="border-radius:0 0 0 6px;">Timeline</div></a>
        <?php
        if($id != $user)
        {
          ?>
          <a href="showAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }
        else {
          ?>
            <a href="editAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }?>

        <a href="friend.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="left:40%;">Friends</div></a>
      </div>

      <div class="boxAbout" style="overflow-y: scroll;">

        <?php
        $name_me_add = new Control;
        $name_me_add_ar = $name_me_add->show_detail_me($user);
        foreach($name_me_add_ar as $row5)
        {
          if(isset($row5->name))
          {
            ?>
            <form method="post" action="database/editMe.php" enctype="multipart/form-data">

              <input type="text" id="nameProfile" placeholder="Name"  name="name" class="about" value="<?php echo $row5->name ?>">
              <br><font style="color:#808080;margin-left:31%;">Profile Image</font><input type="file"  name="fileToUpload" id="fileToUpload" class="about">
              <input type="text" id="email" placeholder="Email" name="email" class="about" value="<?php echo $row5->email ?>">
              <input type="text" id="facebook" placeholder="Facebook" name="fb" class="about" value="<?php echo $row5->fb ?>">
              <input type="hidden" name="user" value="<?php echo $user ?>">
              <input class="savebutton" type="submit" >

            </form>
            <?php
          }
          else {
            ?>
            <form method="post" action="database/addMe.php" enctype="multipart/form-data">

              <input type="text" id="nameProfile" placeholder="Name"  name="name" class="about">
              <br><font style="color:#808080;margin-left:31%;">Profile Image</font><input type="file"  name="fileToUpload" id="fileToUpload" class="about">
              <!-- <br><font style="color:#808080;margin-left:31%;">Cover Image</font><input type="file" id="coverPic" name="fileToUpload" id="fileToUpload2" class="about"> -->
              <input type="text" id="email" placeholder="Email" name="email" class="about">
              <input type="text" id="facebook" placeholder="Facebook" name="fb" class="about">
              <input type="hidden" name="user" value="<?php echo $user ?>">
              <input class="savebutton" type="submit">

            </form>
            <?php
          }
       } ?>
      </div>
    </div>

    <div class="online">
      <?php
      $mysqli = new mysqli('127.0.0.1', 'root', '','social');
      $login = "SELECT * FROM friend WHERE user_id='$user' AND status='yes' ";
      $result = $mysqli->query($login);
        while($row6=$result->fetch_assoc()){
            $friend_id_s = $row6['friend_id'];
            $login_s = "SELECT * FROM members WHERE id='$friend_id_s' AND status ='on' ";
            $result2 = $mysqli->query($login_s);
                while($row7=$result2->fetch_assoc()){
                  ?>   <div class="friendOnline"> <font style="color:green"> &#9679;</font> <?php echo $row7['username']?></div> <?php
                }
        }
       ?>

    </div>

  </body>
</html>
