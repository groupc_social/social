<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'view/comment.php';
include_once 'view/post.php';
include_once 'view/tag.php';
include_once 'view/control.php';
sec_session_start();

$user = $_SESSION['user_id'];
$mysqli = new mysqli('127.0.0.1', 'root', '','social');
?>
<html>
  <head>
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="styleProfile.css">
       <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">


       <script>
           function myFunction2() {
               document.getElementById("myDropdown2").classList.toggle("show2");
           }

           // Close the dropdown menu if the user clicks outside of it
           window.onclick = function(event) {
             if (!event.target.matches('.dropbtn')) {

               var dropdowns = document.getElementsByClassName("dropdown-content2");
               var i;
               for (i = 0; i < dropdowns.length; i++) {
                 var openDropdown = dropdowns[i];
                 if (openDropdown.classList.contains('show2')) {
                   openDropdown.classList.remove('show2');
                 }
               }
             }
           }
       </script>

    <div class="headBar">
      <a href="newfeed.php"><div class="headbtn" style="left:0;width:18.4%;"><image src="logo2.png" style="width:40%;height:90%;margin-top:0.7%;"></div></a>

      <div class="headbtn dropbtn"  onclick="myFunction2()" style="left:18.4%;">add friend
          <div id="myDropdown2" class="dropdown-content2">
            <?php
            $friend_add = new Control;
            $friend_add_ar = $friend_add->add_friend($user);
            foreach($friend_add_ar as $row1)
            {
                $user_friend = $row1->friend_id;
                ?>

                <?php
                $user_friend = "SELECT * FROM me WHERE user_id = $user_friend  ";

                $user_friend_arr = $mysqli->query($user_friend);
                while($row12 = $user_friend_arr->fetch_object()){


                 ?>

                <img src="upload/<?php echo $row12->photo ?>" style="width:20%;height:50%;border-radius:100px;margin-top:3%;">
                <a href="database/addFriend.php?id=<?php echo $user_friend?>"><div style="margin-top:-4%"><?php echo $row12->name ?></div></a><?php
            }
          }
             ?>
          </div>

      </div>

    </div>



  </head>

  <body>
    <div class="menuTab">
      <!-- เเก้ไข้ได้ -->
      <?php
      if(isset($_GET['id']))
      {
      $id = $_GET['id'];
      }
      else {
        $id = $user;
      }
      if($id != $user)
      {
        ?>
        <a href="showAbout.php"><div class="menubtn">About me</div></a>
        <?php
      }
      else {
        ?>
          <a href="editAbout.php"><div class="menubtn">About me</div></a>
        <?php
      }?>
      <a href="profile.php?id=<?php echo $user ?>"><div class="menubtn">My profile</div></a>
      <a href="friend.php?id=<?php echo $user ?>"><div class="menubtn">Friends</div></a>
      <a href="index2.php?id=<?php echo $user ?>"><div class="menubtn">Post movie</div></a>
      <a href=""><div class="menubtn" style="position:absolute;bottom:0;background-color:rgb(149, 149, 149);border-radius:0;">Log Out</div></a>
    </div>
    <div class="profile">
      <?php
      $name_me = new Control;
      $name_me_ar = $name_me->show_detail_me($user);
      foreach($name_me_ar as $row)
      {
      ?>
      <!-- ////////////////////////////////////// -->
      <div class="headProfile"> Cover Photo</b>
        <div class="profilePic">
        <img src="upload/<?php echo $row->photo?> " alt="Nature" style="width:100%">
        </div>
        <a href="#profile"><div id="name"><?php echo $row->name?></div></a>
          <?php } ?>
      </div>

      <div class="menuProfile">
        <a href="profile.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="border-radius:0 0 0 6px;">Timeline</div></a>
        <?php
        if($id != $user)
        {
          ?>
          <a href="showAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }
        else {
          ?>
            <a href="editAbout.php"><div class="menuProfilebtn" style="left:20%;">About</div></a>
          <?php
        }?>

        <a href="friend.php?id=<?php echo $user ?>"><div class="menuProfilebtn" style="left:40%;">Friends</div></a>

      </div>

      <div class="post2">
        <?php
        $show_all_comment = new Control;
        $show_all_comment_arr = $show_all_comment->show_message($user,$id);
        foreach($show_all_comment_arr as $row90)
        {
            if($row90->user_id == $user){
              ?>
              <div class="user1">
                <?php
                echo $row90->message.'<br>'.'<br>';
                 ?>
              </div>
                <br>
              <?php
            }
            else {
              ?>
              <div class="user2">
                <?php
                echo $row90->message.'<br>'.'<br>';
                 ?>
              </div>
              <br>
              <?php
            }
        }

         ?>
         <style>
          .user1{
              text-align: center;
              font-size: 1.4em;
              display: block;
              width: 30%;
              margin-right: 0%;
              margin-left: 65%;
              margin-top: 2%;
              background-color: pink;
              margin-bottom: -2%;
              border-radius: 10px;
          }
          .user2{
            text-align: center;

            font-size: 1.4em;
            border-radius: 10px;
            margin-left: 5%;
              display: block;
              margin-bottom: -2%;
              margin-top: 2%;
              width: 30%;
              background-color: lightblue;
          }

          input#post3{
            position: absolute;
            font-size: 1.3em;
            top: 92%;
            left: 27%;
            border: none;
            width: 45%;
            height: 5%;
            border-bottom: 1px solid #bfbfbf;
            background-color: #FEF9E7;
          }
          .button4{
            position: absolute;
            border: 2px solid #7a7a7a;
            background-color: #F5B7B1;
            color: rgb(75, 75, 75);
            border-radius: 3px;
            color: black;
            width: 10.5%;
            height: 5%;
            right: 17.9%;
            bottom: 3%;
            text-align: center;
          }
          .button4:hover{
            background-color: #7a7a7a;
            color: white;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
          }
         </style>
         </div>

        <form method="post" action="database/insertMessage.php">
          <input type="text" placeholder=" write a post" id="post3" name="detail">
          <input type="hidden" name="user" value="<?php echo $user ?>">
            <input type="hidden" name="friend" value="<?php echo $id ?>">
          <input class="button4" type="submit">
        </form>

    </div>
    <br><br>

    <div class="online">
      <?php
      $mysqli = new mysqli('127.0.0.1', 'root', '','social');
      $login = "SELECT * FROM friend WHERE user_id='$user' AND status='yes' ";
      $result = $mysqli->query($login);
        while($row6=$result->fetch_assoc()){
            $friend_id_s = $row6['friend_id'];
            $login_s = "SELECT * FROM members WHERE id='$friend_id_s' AND status ='on' ";
            $result2 = $mysqli->query($login_s);
                while($row7=$result2->fetch_assoc()){
                  ?>   <a href="message.php?id=<?php echo $row7['id'] ?> "><div class="friendOnline"> <font style="color:green"> &#9679;</font> <?php echo $row7['username']?></div></a> <?php
                }
        }
       ?>

    </div>

  </body>
  <style>/* Post in Profile page*/

  .showPost{
    margin-top: 50%;
    margin-left: 13%;
    width: 80%;
  }

  .eachPost{
    margin-top: 5%;
    margin-left: 10%;
    background-color: #d6d6d6;
    width: 85%;
    border-radius: 10px;
    padding: 5%;
  }
  /* About me page */
  .nameMovie{
      /*position: absolute;*/
      height: 7%;
      width: 81%;
      background: rgb(220, 222, 222);
      /*border-radius: 5px 5px 5px 5px;*/
      margin-top: 10%;
      margin-left: 0%;
      box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
  }
  .postMovie{
      /*position: absolute;*/
      background: rgb(221, 221, 221);
      box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
      width: 81%;
      height: 54%;
      margin-top: 4%;
      margin-left: 0%;
  }
  .postmassage{
      /*position: absolute;*/
      background: rgb(221, 221, 221);
      box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
      width: 81%;
      height: 7%;
      margin-top: 4%;
      margin-left: 0%;
  }
  .Rate{
      /*position: absolute;*/
      background: rgb(221, 221, 221);
      box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
      width: 59%;
      height: 7%;
      margin-top: 5%;
      margin-left: -22%;
  }

  .Tag{
      /*position: absolute;*/
      background: rgb(221, 221, 221);
      box-shadow: 1px 1px 3px 3px rgba(30,30,30,.2);
      width: 20%;
      height: 7%;
      margin-top: -7%;
      margin-left: 62%;
      cursor: pointer;
      }
      </style>
      <script>
        $(document).ready(function() {
          $('.manuED').click(function() {
              $(this).next().toggle();
              // $('.myDropdown').show()
          });

          $('.post').hover(
              function(){
                  $(this).find('.manuED').show();
              },
              function(){
                  $(this).find('.manuED').hide();
              }
          );
          $('.edit').click(
              function(){
              $('.myDropdown').hide();
          });
          $('.delete').click(
              function(){
              $('.myDropdown').hide();
          });
        });
      </script>
      <script>

          // Close the dropdown if the user clicks outside of it
          window.onclick = function(event) {
            if (!event.target.matches('.manuED')) {

              var dropdowns = document.getElementsByClassName("dropdown-content");
              var i;
              for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                  openDropdown.classList.remove('show');

                }
              }
            }
          }
      </script>
</html>
