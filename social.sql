-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2016 at 08:17 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `social`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `post_id`, `comment`) VALUES
(7, 2, 8, 'sjbhfbhdv'),
(8, 2, 8, 'bomsuay'),
(9, 2, 8, 'sofia'),
(10, 2, 8, 'na'),
(11, 2, 8, 'Wowwwwwww!!!!'),
(12, 2, 10, 'à¸—à¸”à¸ªà¸­à¸šà¸šà¸š'),
(13, 4, 8, 'à¸—à¸”à¸ªà¸­à¸šà¸šà¸šà¸šà¸š'),
(14, 1, 8, 'à¸—à¸”à¸ªà¸­à¸šà¸šà¸šà¸šà¸š'),
(15, 2, 15, 'kkko'),
(16, 2, 17, 'lol');

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE `friend` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `status` text NOT NULL,
  `addfriend` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friend`
--

INSERT INTO `friend` (`id`, `user_id`, `friend_id`, `status`, `addfriend`) VALUES
(7, 4, 2, 'yes', 4),
(8, 2, 4, 'yes', 4),
(18, 2, 1, 'yes', 1),
(19, 1, 2, 'yes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(2, '1481644304'),
(4, '1481805029'),
(4, '1481848449'),
(2, '1481849939'),
(2, '1481854968'),
(7, '1481861117'),
(1, '1481861705');

-- --------------------------------------------------------

--
-- Table structure for table `me`
--

CREATE TABLE `me` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `photo` text NOT NULL,
  `email` text NOT NULL,
  `fb` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `me`
--

INSERT INTO `me` (`id`, `name`, `photo`, `email`, `fb`, `user_id`) VALUES
(2, 'illumillal', '13925032_516009651926720_5229263359200885879_n.jpg', 'poramin.o21@hotmail.com', 'https://www.facebook.com/illumillal', 2),
(5, 'phonecyber', '740294.png', 'asdsdasdsad', 'asdsadsadasdasdd', 4),
(9, 'bom', 'bom.jpg', 'bom@hotmail.com', 'Supapitch Thungtragul', 1);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`, `status`) VALUES
(1, 'bom', 'poramin.o21@hotmail.com', '299e09cbb258d06de91f76c3e5b7e490d7b9fa0d7afac741ece6fb2bc7e172c6238bb3fdcf84ceec181013d792db85af30b9107c2729e65e407ffc3c3d302847', '1041390d43af5c3c2240b15e3be2a1b10080cafee37734a0446dd9d775e10d1c2c3ae332777414b2444c0236e74b21c3260412d215ad5c1ece850be90333cded', 'no'),
(2, 'illumillal', 'a@hotmail.com', '4300a2a8602dbe7281cf9ed7644b386efc32f922b7739874aab61ca4c1442a2f41fa30a93f28558644699d27fde2a54e31cc68e67addf61ef0052bc51b1b5d48', '053507ee211a9b0ce1b88d0b4058919cd08df04807eedc9f7b78fb5a71dcd34c0fd9c012ce42bfbc3e88223ac8d15fb78836683b1900d8d46d60be7063ddb4e3', 'on'),
(4, 'phonecyber', 'c@hotmail.com', 'bb5d41f86bbbe6e06499c01e52edd4e16bc7fb333dd45766021e42898d6fd864e7b7f419e74f87b71bb970d87f81ca5032cacb015ef12f5d66fe0adbedaeb380', '876a4ecc4727827785c1f1bc4e3d15b9e4457d49ca403df6b848ceb006716da704285ef6dca252796909f50beb4893fb32b314c403a3e9d66a945cffa24608e5', 'no'),
(5, 'NaPnp', 'na@hotmail.com', '126d444097a175a669501bbea250b0dd90a77da55eb81079647bbc72a1b912b7bcb41167360dabd33f3714c29be5081cc37af950eab454bec71409292e9cdb80', 'fa57917a767d0fed792b3d3553801da04f8f3af794e330395b31d6d711f070841bf9d2800e05cc0539c2858973fa4427f76948f031924c1ca2082cb5a9b31660', 'no'),
(7, 'phonecyber3', 'phonecyber999@hotmail.com', 'dc13ec514c492a0fcaa479bd17f97c546a757760198691d186e8ddcd665d0343c2b1d267de79e8a03af387e67e0fbdc67d922aa9dcb0a756fb60372ea7a9e408', 'b537cfbd73509e6119d99d3ce05a9d451caf2e3b77fb9684ca8e0e687a52ce3e7cd405c3795fc3c51273036afe9443291244f7cea32a7930289549b4d9a9c49b', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `friend_id`, `message`, `user_id`) VALUES
(1, 2, 'sdfsdfdf', 1),
(2, 1, 'dfdsfsdf', 2),
(3, 3, 'fdfdfdf', 2),
(5, 2, 'dsdsdsd', 3),
(6, 2, 'dfdfdfdf', 3),
(7, 2, 'dfdfdf', 3),
(8, 4, 'dsdsdsd', 2),
(15, 4, '2', 2),
(16, 4, '3', 2),
(17, 4, 'bombam5555', 2),
(18, 4, 'nooooooo', 2),
(19, 2, 'test', 4),
(20, 4, 'eiei', 2),
(21, 4, 'jub jub', 2),
(22, 2, 'hhohohoho', 4),
(23, 1, 'hooo', 2);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `detail` text NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `detail`, `date`, `user_id`, `table_name`) VALUES
(1, 'wdwdwdd', '2016-12-15', 2, 'post'),
(2, 'vxdfdfsdfsdf', '0000-00-00', 0, ''),
(3, '', '0000-00-00', 0, ''),
(4, 'test', '0000-00-00', 0, ''),
(5, 'sofia', '0000-00-00', 0, ''),
(6, 'bom', '0000-00-00', 0, ''),
(7, 'sdsdsdssd', '0000-00-00', 0, ''),
(8, 'sdsdsdssddsdsd', '0000-00-00', 0, ''),
(9, 'As promised, the title critters in Fantastic Beasts are whimsically entertaining and occasionally as entrancing as those animals crawling through a medieval illuminated manuscript', '0000-00-00', 2, 'post'),
(10, 'Fantastic Beasts and Where to Find Them', '0000-00-00', 2, 'post'),
(11, 'hhhhhhpppp', '0000-00-00', 2, 'post');

-- --------------------------------------------------------

--
-- Table structure for table `post_admin`
--

CREATE TABLE `post_admin` (
  `id` int(11) NOT NULL,
  `detail` text NOT NULL,
  `photo` text NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_admin`
--

INSERT INTO `post_admin` (`id`, `detail`, `photo`, `title`) VALUES
(1, 'When a crime brings them back to the mean streets of Los Angeles, fugitive ex-convict Dom Toretto (Vin Diesel) and agent Brian O''Conner (Paul Walker) reignite their high-octane feud. However, when a common enemy rears his head, Dom and Brian must learn how to work together and trust one another in order to defeat him.', 'fast_and_the_furious_4.jpeg.jpg', 'Fast & Furious 4'),
(2, 'The year is 1926, and Newt Scamander (Eddie Redmayne) has just completed a global excursion to find and document an extraordinary array of magical creatures. Arriving in New York for a brief stopover, he might have come and gone without incident, were it not for a No-Maj (American for Muggle) named Jacob, a misplaced magical case, and the escape of some of Newt''s fantastic beasts, which could spell trouble for both the wizarding and No-Maj worlds.', 'fantastic-beasts-review-21nov16.jpg', 'Fantastic Beasts and Where to Find Them'),
(3, 'Transformers: The Last Knight is an upcoming 2017 American science fiction-action film based on the Transformers toy line. ', 'Transformers-5-The-Last-Knight-Looking-for-Men-Women.jpg', 'Transformers: The Last Knight'),
(4, 'When Tony Stark (Robert Downey Jr.) jump-starts a dormant peacekeeping program, things go terribly awry, forcing him, Thor (Chris Hemsworth), the Incredible Hulk (Mark Ruffalo) and the rest of the Avengers to reassemble. As the fate of Earth hangs in the balance, the team is put to the ultimate test as they battle Ultron, a technological terror hell-bent on human extinction. Along the way, they encounter two mysterious and powerful newcomers, Pietro and Wanda Maximoff.', 'Avengers-Age-of-Ultron.jpeg', 'Avengers: Age of Ultron'),
(5, 'The day the stars fell, two lives changed forever. High schoolers Mitsuha and Taki are complete strangers living separate lives. But one night, they suddenly switch places. Mitsuha wakes up in Taki''s body, and he in hers. This bizarre occurrence continues to happen randomly, and the two must adjust their lives around each other. Yet, somehow, it works. They build a connection and communicate by leaving notes, messages, and more importantly, an imprint. When a dazzling comet lights up the night''s sky, something shifts, and they seek each other out wanting something more - a chance to finally meet.', 'Your-Name.-Shinkai-Makoto-04.png', 'Your Name'),
(6, 'After his career is destroyed, a brilliant but arrogant surgeon gets a new lease on life when a sorcerer takes him under his wing and trains him to defend the world against evil.', 'Screen_Shot_2016-07-15_at_3.40.37_AM_3.png', 'Doctor Strange'),
(7, 'In Earth''s future, a global crop blight and second Dust Bowl are slowly rendering the planet uninhabitable. Professor Brand (Michael Caine), a brilliant NASA physicist, is working on plans to save mankind by transporting Earth''s population to a new home via a wormhole. But first, Brand must send former NASA pilot Cooper (Matthew McConaughey) and a team of researchers through the wormhole and across the galaxy to find out which of three planets could be mankind''s new home.', 'interstellar-movie.jpg', 'Interstellar');

-- --------------------------------------------------------

--
-- Table structure for table `post_movie`
--

CREATE TABLE `post_movie` (
  `id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `spoil` text NOT NULL,
  `rate` int(11) NOT NULL,
  `detail` text NOT NULL,
  `date` date NOT NULL,
  `table_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_movie`
--

INSERT INTO `post_movie` (`id`, `movie_id`, `user_id`, `spoil`, `rate`, `detail`, `date`, `table_name`) VALUES
(6, 7, 3, 'Your name', 0, 'Part body-swap comedy, part long-distance romance, part... something else. If you only see one Japanese animated feature this year, see this one, and see it more than once.', '2016-12-15', 'post_movie'),
(8, 2, 2, 'Fantastic Beasts ', 5, 'As promised, the title critters in Fantastic Beasts are whimsically entertaining and occasionally as entrancing as those animals crawling through a medieval illuminated manuscript', '0000-00-00', 'post_movie'),
(12, 3, 2, 'Wowwwww', 5, 'Marvel''s most satisfying entry since "Spider-Man 2," and a throwback to M. Night Shyamalan''s soul-searching identity-crisis epic "Unbreakable,', '0000-00-00', 'post_movie'),
(15, 5, 4, 'Woww!!', 5, 'A super-sized spandex soap opera that''s heavy on catastrophic action but surprisingly light on its feet, and rich in the human-scale emotion that can cut even a raging Hulk down to size.', '2016-12-16', 'post_movie'),
(16, 4, 2, 'Woww!!!!', 5, 'Homwork', '2016-12-16', 'post_movie'),
(17, 7, 4, 'Godddd!!', 5, 'Brainy, barmy and beautiful to behold, this is Stephen Hawking’s Star Trek: a mind-bending opera of space and time with a soul wrapped up in all the science.', '2016-12-16', 'post_movie'),
(18, 6, 2, 'Woww!!!!', 5, 'Wowwwww', '0000-00-00', 'post_movie');

-- --------------------------------------------------------

--
-- Table structure for table `tag_movie`
--

CREATE TABLE `tag_movie` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tag_user`
--

CREATE TABLE `tag_user` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag_user`
--

INSERT INTO `tag_user` (`id`, `post_id`, `user_id`, `tag`) VALUES
(3, 1, 2, 'movie'),
(4, 1, 2, 'yourname'),
(5, 2, 2, ''),
(6, 3, 2, ''),
(7, 4, 2, ''),
(8, 5, 2, 'ombam'),
(9, 6, 2, 'ia'),
(10, 7, 2, 'movie'),
(11, 7, 2, 'yourname'),
(12, 8, 2, 'movie'),
(13, 8, 2, 'yourname'),
(14, 9, 2, 'movie'),
(15, 9, 2, 'yourname'),
(16, 10, 2, 'movie'),
(17, 10, 2, 'yourname'),
(18, 11, 2, 'movie'),
(19, 11, 2, 'yourname');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `me`
--
ALTER TABLE `me`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_admin`
--
ALTER TABLE `post_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_movie`
--
ALTER TABLE `post_movie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_movie`
--
ALTER TABLE `tag_movie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_user`
--
ALTER TABLE `tag_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `friend`
--
ALTER TABLE `friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `me`
--
ALTER TABLE `me`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `post_admin`
--
ALTER TABLE `post_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `post_movie`
--
ALTER TABLE `post_movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tag_movie`
--
ALTER TABLE `tag_movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag_user`
--
ALTER TABLE `tag_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
